#include "WiFi.h"
#include <ESPAsyncWebServer.h>

#include <esp_wifi.h>
#include <esp_now.h>

const char *ssid = "ESP32_Splicer_Board";
const char *password = "123456789";

uint8_t newMACAddress[] = {0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x00};

AsyncWebServer server(80);
#include "index.h" //Web page header file

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message
{
    char a[32];
    int b;
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len)
{
    memcpy(&myData, incomingData, sizeof(myData));
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Char: ");
    Serial.println(myData.a);
    Serial.print("Int: ");
    Serial.println(myData.b);
    Serial.println();

    PressButton(myData.b);
}

void setup()
{
    Serial.begin(115200);
    InitPins();

    WiFi.mode(WIFI_AP_STA);

    // Set device as a Wi-Fi Station
    WiFi.begin(ssid, password);

    Serial.println();
    Serial.print("Wi-Fi Channel: ");
    Serial.println(WiFi.channel());
    Serial.print("IP address: ");
    Serial.println(WiFi.softAPIP());

    esp_wifi_set_mac(WIFI_IF_STA, &newMACAddress[0]);

    Serial.print("[NEW] ESP32 Board MAC Address:  ");
    Serial.println(WiFi.macAddress());

    // Init ESP-NOW
    if (esp_now_init() != ESP_OK)
    {
        Serial.println("Error initializing ESP-NOW");
        return;
    }

    // Once ESPNow is successfully Init, we will register for recv CB to
    // get recv packer info
    esp_now_register_recv_cb(OnDataRecv);

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String s = MAIN_page;
                  request->send(200, "text/html", s);
              });

    server.on("/btn0", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(0);
                  Serial.println("Go to floor 0");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn1", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(1);
                  Serial.println("Go to floor 1");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn2", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(2);
                  Serial.println("Go to floor 2");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn3", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(3);
                  Serial.println("Go to floor 3");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn4", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(4);
                  Serial.println("Go to floor 4");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn5", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(5);
                  Serial.println("Go to floor 5");
                  request->send(200, "text/html", "1");
              });
    server.on("/btnBell", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(6);
                  Serial.println("Ring Bell");
                  request->send(200, "text/html", "1");
              });
            

    server.on("/readADC", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  UpdateLEDStates();

                  // Create JSON array holding all LED states
                  String adcValue = "{\"values\":[";
                  for (int i = 0; i < 10; i++)
                  {
                      adcValue += String(GetSensePinState(i));

                      if (i != 9)
                          adcValue += ",";
                  }
                  adcValue += "]}";
                  Serial.println(adcValue);

                  request->send(200, "text/plain", adcValue);
              });

    server.begin();
}

void loop()
{
    UpdateButtons();
    UpdateOTA();
}
