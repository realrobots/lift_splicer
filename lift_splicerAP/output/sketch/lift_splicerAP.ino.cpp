#include <Arduino.h>
#line 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
#include "WiFi.h"
#include <ESPAsyncWebServer.h>

#include <esp_wifi.h>
#include <esp_now.h>

const char *ssid = "ESP32_Splicer_Board";
const char *password = "123456789";

uint8_t newMACAddress[] = {0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x00};

AsyncWebServer server(80);
#include "index.h" //Web page header file

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message
{
    char a[32];
    int b;
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback function that will be executed when data is received
#line 27 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len);
#line 41 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
void setup();
#line 145 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
void loop();
#line 29 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
void InitPins();
#line 42 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
void UpdateLEDStates();
#line 50 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
uint8_t GetSensePinState(uint8_t idx);
#line 55 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
void PressButton(uint8_t btnIdx);
#line 65 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
void UpdateButtons();
#line 6 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino"
void initOTA();
#line 54 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino"
void UpdateOTA();
#line 27 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len)
{
    memcpy(&myData, incomingData, sizeof(myData));
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Char: ");
    Serial.println(myData.a);
    Serial.print("Int: ");
    Serial.println(myData.b);
    Serial.println();

    PressButton(myData.b);
}

void setup()
{
    Serial.begin(115200);
    InitPins();

    WiFi.mode(WIFI_AP_STA);

    // Set device as a Wi-Fi Station
    WiFi.begin(ssid, password);

    Serial.println();
    Serial.print("Wi-Fi Channel: ");
    Serial.println(WiFi.channel());
    Serial.print("IP address: ");
    Serial.println(WiFi.softAPIP());

    esp_wifi_set_mac(WIFI_IF_STA, &newMACAddress[0]);

    Serial.print("[NEW] ESP32 Board MAC Address:  ");
    Serial.println(WiFi.macAddress());

    // Init ESP-NOW
    if (esp_now_init() != ESP_OK)
    {
        Serial.println("Error initializing ESP-NOW");
        return;
    }

    // Once ESPNow is successfully Init, we will register for recv CB to
    // get recv packer info
    esp_now_register_recv_cb(OnDataRecv);

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String s = MAIN_page;
                  request->send(200, "text/html", s);
              });

    server.on("/btn0", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(0);
                  Serial.println("Go to floor 0");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn1", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(1);
                  Serial.println("Go to floor 1");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn2", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(2);
                  Serial.println("Go to floor 2");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn3", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(3);
                  Serial.println("Go to floor 3");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn4", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(4);
                  Serial.println("Go to floor 4");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn5", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(5);
                  Serial.println("Go to floor 5");
                  request->send(200, "text/html", "1");
              });
    server.on("/btnBell", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(6);
                  Serial.println("Ring Bell");
                  request->send(200, "text/html", "1");
              });
            

    server.on("/readADC", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  UpdateLEDStates();

                  // Create JSON array holding all LED states
                  String adcValue = "{\"values\":[";
                  for (int i = 0; i < 10; i++)
                  {
                      adcValue += String(GetSensePinState(i));

                      if (i != 9)
                          adcValue += ",";
                  }
                  adcValue += "]}";
                  Serial.println(adcValue);

                  request->send(200, "text/plain", adcValue);
              });

    server.begin();
}

void loop()
{
    UpdateButtons();
    UpdateOTA();
}

#line 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
#define LED_GREEN 35
#define LED_RED 36
#define LED_BLUE 39
#define LED_INFO 34
#define LED_FLOOR_0 22
#define LED_FLOOR_1 23
#define LED_FLOOR_2 25
#define LED_FLOOR_3 26
#define LED_FLOOR_4 27
#define LED_FLOOR_5 32

#define BTN_FLOOR_0 13
#define BTN_FLOOR_1 16
#define BTN_FLOOR_2 17
#define BTN_FLOOR_3 18
#define BTN_FLOOR_4 19
#define BTN_FLOOR_5 21
#define BTN_BELL 4

#define BTN_PUSH_PERIOD 200 //ms

uint8_t buttonPins[] = {BTN_FLOOR_0, BTN_FLOOR_1, BTN_FLOOR_2, BTN_FLOOR_3, BTN_FLOOR_4, BTN_FLOOR_5, BTN_BELL};
uint8_t sensePins[] = {LED_GREEN, LED_RED, LED_BLUE, LED_INFO, LED_FLOOR_0, LED_FLOOR_1, LED_FLOOR_2, LED_FLOOR_3, LED_FLOOR_4, LED_FLOOR_5};
uint8_t sensePinStates[10];

uint8_t btnDown[10];
long btnDownTime[10];

void InitPins()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        pinMode(buttonPins[i], OUTPUT);
        digitalWrite(buttonPins[i], HIGH);
    }
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        pinMode(sensePins[i], INPUT);
    }
}

void UpdateLEDStates()
{
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        sensePinStates[i] = digitalRead(sensePins[i]);
    }
}

uint8_t GetSensePinState(uint8_t idx)
{
    return sensePinStates[idx];
}

void PressButton(uint8_t btnIdx)
{
    digitalWrite(buttonPins[btnIdx], LOW);
    btnDown[btnIdx] = 1;
    btnDownTime[btnIdx] = millis();
    Serial.print("button ");
    Serial.print(btnIdx);
    Serial.println(" down");
}

void UpdateButtons()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        if (btnDown[i] && millis() - btnDownTime[i] > BTN_PUSH_PERIOD)
        {
            digitalWrite(buttonPins[i], HIGH);
            btnDown[i] = 0;
            Serial.print("button ");
            Serial.print(i);
            Serial.println(" up");
        }
    }
}

#line 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino"

#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

void initOTA() {

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  ArduinoOTA.setPassword("realrobots");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void UpdateOTA(){
    
}
