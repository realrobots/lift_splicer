const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
<style>
.button {
  background-color: #9e9e9e;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border: 2px solid black;
}
.button:active{
  background-color: #4f4f4f;
}
</style>
<body>

<div >
<button class="button" id="btn_Floor0">Floor 0</button>
<button class="button" id="btn_Floor1">Floor 1</button>
<button class="button" id="btn_Floor2">Floor 2</button>
<button class="button" id="btn_Floor3">Floor 3</button>
<button class="button" id="btn_Floor4">Floor 4</button>
<button class="button" id="btn_Floor5">Floor 5</button>
<button class="button" id="btn_Bell">Bell</button>

  <p>LED GREEN:<span id="ledStateGreen">0</span></p><br>
  <p>LED RED:<span id="ledStateRed">0</span></p><br>
  <p>LED BLUE:<span id="ledStateBlue">0</span></p><br>
  <p>LED INFO:<span id="ledStateInfo">0</span></p><br>
  <p>LED FLOOR 0:<span id="ledStateFloor0">0</span></p><br>
  <p>LED FLOOR 1:<span id="ledStateFloor1">0</span></p><br>
  <p>LED FLOOR 2:<span id="ledStateFloor2">0</span></p><br>
  <p>LED FLOOR 3:<span id="ledStateFloor3">0</span></p><br>
  <p>LED FLOOR 4:<span id="ledStateFloor4">0</span></p><br>
  <p>LED FLOOR 5:<span id="ledStateFloor5">0</span></p><br>
</div>
<script>

function GotoFloor0(){
    console.log("floor0");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn0", true);
  xhttp.send();
}
function GotoFloor1(){
    console.log("floor1");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn1", true);
  xhttp.send();
}
function GotoFloor2(){
    console.log("floor2");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn2", true);
  xhttp.send();
}
function GotoFloor3(){
    console.log("floor3");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn3", true);
  xhttp.send();
}
function GotoFloor4(){
    console.log("floor4");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn4", true);
  xhttp.send();
}
function GotoFloor5(){
    console.log("floor5");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btn5", true);
  xhttp.send();
}
function GotoBell(){
    console.log("bell");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
    }
  };
  xhttp.open("GET", "btnBell", true);
  xhttp.send();
}

document.getElementById("btn_Floor0").addEventListener("click", GotoFloor0);
document.getElementById("btn_Floor1").addEventListener("click", GotoFloor1);
document.getElementById("btn_Floor2").addEventListener("click", GotoFloor2);
document.getElementById("btn_Floor3").addEventListener("click", GotoFloor3);
document.getElementById("btn_Floor4").addEventListener("click", GotoFloor4);
document.getElementById("btn_Floor5").addEventListener("click", GotoFloor5);
document.getElementById("btn_Bell").addEventListener("click", GotoBell);

setInterval(function() {
  // Call a function repetatively with 2 Second interval
  getData();
}, 100); //2000mSeconds update rate

function getData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("ADCValue").innerHTML =
      //this.responseText;
      
      var ledState = JSON.parse(this.responseText);
      //console.log(ledState);

      document.getElementById("ledStateGreen").innerHTML = ledState.values[0];
      document.getElementById("ledStateRed").innerHTML = ledState.values[1];
      document.getElementById("ledStateBlue").innerHTML = ledState.values[2];
      document.getElementById("ledStateInfo").innerHTML = ledState.values[3];
      document.getElementById("ledStateFloor0").innerHTML = ledState.values[4];
      document.getElementById("ledStateFloor1").innerHTML = ledState.values[5];
      document.getElementById("ledStateFloor2").innerHTML = ledState.values[6];
      document.getElementById("ledStateFloor3").innerHTML = ledState.values[7];
      document.getElementById("ledStateFloor4").innerHTML = ledState.values[8];
      document.getElementById("ledStateFloor5").innerHTML = ledState.values[9];
    }
  };
  xhttp.open("GET", "readADC", true);
  xhttp.send();
}
</script>
</body>
</html>
)=====";