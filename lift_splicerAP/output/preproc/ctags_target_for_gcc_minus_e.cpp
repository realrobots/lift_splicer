# 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino"
# 2 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino" 2
# 3 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino" 2

# 5 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino" 2
# 6 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino" 2

const char *ssid = "ESP32_Splicer_Board";
const char *password = "123456789";

uint8_t newMACAddress[] = {0x32, 0xAE, 0xA4, 0x07, 0x0D, 0x00};

AsyncWebServer server(80);
# 14 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\lift_splicerAP.ino" 2

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message
{
    char a[32];
    int b;
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len)
{
    memcpy(&myData, incomingData, sizeof(myData));
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Char: ");
    Serial.println(myData.a);
    Serial.print("Int: ");
    Serial.println(myData.b);
    Serial.println();

    PressButton(myData.b);
}

void setup()
{
    Serial.begin(115200);
    InitPins();

    WiFi.mode(WIFI_MODE_APSTA);

    // Set device as a Wi-Fi Station
    WiFi.begin(ssid, password);

    Serial.println();
    Serial.print("Wi-Fi Channel: ");
    Serial.println(WiFi.channel());
    Serial.print("IP address: ");
    Serial.println(WiFi.softAPIP());

    esp_wifi_set_mac(WIFI_IF_STA, &newMACAddress[0]);

    Serial.print("[NEW] ESP32 Board MAC Address:  ");
    Serial.println(WiFi.macAddress());

    // Init ESP-NOW
    if (esp_now_init() != 0 /*!< esp_err_t value indicating success (no error) */)
    {
        Serial.println("Error initializing ESP-NOW");
        return;
    }

    // Once ESPNow is successfully Init, we will register for recv CB to
    // get recv packer info
    esp_now_register_recv_cb(OnDataRecv);

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  String s = MAIN_page;
                  request->send(200, "text/html", s);
              });

    server.on("/btn0", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(0);
                  Serial.println("Go to floor 0");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn1", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(1);
                  Serial.println("Go to floor 1");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn2", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(2);
                  Serial.println("Go to floor 2");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn3", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(3);
                  Serial.println("Go to floor 3");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn4", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(4);
                  Serial.println("Go to floor 4");
                  request->send(200, "text/html", "1");
              });
    server.on("/btn5", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(5);
                  Serial.println("Go to floor 5");
                  request->send(200, "text/html", "1");
              });
    server.on("/btnBell", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  PressButton(6);
                  Serial.println("Ring Bell");
                  request->send(200, "text/html", "1");
              });


    server.on("/readADC", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  UpdateLEDStates();

                  // Create JSON array holding all LED states
                  String adcValue = "{\"values\":[";
                  for (int i = 0; i < 10; i++)
                  {
                      adcValue += String(GetSensePinState(i));

                      if (i != 9)
                          adcValue += ",";
                  }
                  adcValue += "]}";
                  Serial.println(adcValue);

                  request->send(200, "text/plain", adcValue);
              });

    server.begin();
}

void loop()
{
    UpdateButtons();
    UpdateOTA();
}
# 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
# 22 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\gpio.ino"
uint8_t buttonPins[] = {13, 16, 17, 18, 19, 21, 4};
uint8_t sensePins[] = {35, 36, 39, 34, 22, 23, 25, 26, 27, 32};
uint8_t sensePinStates[10];

uint8_t btnDown[10];
long btnDownTime[10];

void InitPins()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        pinMode(buttonPins[i], 0x02);
        digitalWrite(buttonPins[i], 0x1);
    }
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        pinMode(sensePins[i], 0x01);
    }
}

void UpdateLEDStates()
{
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        sensePinStates[i] = digitalRead(sensePins[i]);
    }
}

uint8_t GetSensePinState(uint8_t idx)
{
    return sensePinStates[idx];
}

void PressButton(uint8_t btnIdx)
{
    digitalWrite(buttonPins[btnIdx], 0x0);
    btnDown[btnIdx] = 1;
    btnDownTime[btnIdx] = millis();
    Serial.print("button ");
    Serial.print(btnIdx);
    Serial.println(" down");
}

void UpdateButtons()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        if (btnDown[i] && millis() - btnDownTime[i] > 200 /*ms*/)
        {
            digitalWrite(buttonPins[i], 0x1);
            btnDown[i] = 0;
            Serial.print("button ");
            Serial.print(i);
            Serial.println(" up");
        }
    }
}
# 1 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino"

# 3 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino" 2
# 4 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino" 2
# 5 "e:\\nextcloud\\Projects\\LiftRibbonSplice\\firmware\\lift_splicerAP\\ota.ino" 2

void initOTA() {

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  ArduinoOTA.setPassword("realrobots");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == 0)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void UpdateOTA(){

}
