#define LED_GREEN 35
#define LED_RED 36
#define LED_BLUE 39
#define LED_INFO 34
#define LED_FLOOR_0 22
#define LED_FLOOR_1 23
#define LED_FLOOR_2 25
#define LED_FLOOR_3 26
#define LED_FLOOR_4 27
#define LED_FLOOR_5 32

#define BTN_FLOOR_0 13
#define BTN_FLOOR_1 16
#define BTN_FLOOR_2 17
#define BTN_FLOOR_3 18
#define BTN_FLOOR_4 19
#define BTN_FLOOR_5 21
#define BTN_BELL 4

#define BTN_PUSH_PERIOD 200 //ms

uint8_t buttonPins[] = {BTN_FLOOR_0, BTN_FLOOR_1, BTN_FLOOR_2, BTN_FLOOR_3, BTN_FLOOR_4, BTN_FLOOR_5, BTN_BELL};
uint8_t sensePins[] = {LED_GREEN, LED_RED, LED_BLUE, LED_INFO, LED_FLOOR_0, LED_FLOOR_1, LED_FLOOR_2, LED_FLOOR_3, LED_FLOOR_4, LED_FLOOR_5};
uint8_t sensePinStates[10];

uint8_t btnDown[10];
long btnDownTime[10];

void InitPins()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        pinMode(buttonPins[i], OUTPUT);
        digitalWrite(buttonPins[i], HIGH);
    }
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        pinMode(sensePins[i], INPUT);
    }
}

void UpdateLEDStates()
{
    for (int i = 0; i < sizeof(sensePins) / sizeof(uint8_t); i++)
    {
        sensePinStates[i] = digitalRead(sensePins[i]);
    }
}

uint8_t GetSensePinState(uint8_t idx)
{
    return sensePinStates[idx];
}

void PressButton(uint8_t btnIdx)
{
    digitalWrite(buttonPins[btnIdx], LOW);
    btnDown[btnIdx] = 1;
    btnDownTime[btnIdx] = millis();
    Serial.print("button ");
    Serial.print(btnIdx);
    Serial.println(" down");
}

void UpdateButtons()
{
    for (int i = 0; i < sizeof(buttonPins) / sizeof(uint8_t); i++)
    {
        if (btnDown[i] && millis() - btnDownTime[i] > BTN_PUSH_PERIOD)
        {
            digitalWrite(buttonPins[i], HIGH);
            btnDown[i] = 0;
            Serial.print("button ");
            Serial.print(i);
            Serial.println(" up");
        }
    }
}
